Identity Fraud from Enron - Utilizing machine learning techniques identify persons of interest(POI) in the dataset
	from the 2011 Enron Scandals. Using the database of emails and other financial data on individuals in 
	the corporation we want to apply multiple machine learning algorithms to create a classifier that will identify 
	any POI that may be involved in the scandal.

Investigating a Dataset - This project analyzes a dataset utilizing the pandas, NumPy, Matplotlib, and csv python libraries. 
	The final analysis uses Jupyter notebook to create a report for presentation. The dataset used
	in this analysis is included with the name, 'noshowappointments-kagglev2-may-2016.csv'

Testing a Perceptual Phenomenon - Use descriptive statistics and a statistical test (T-score) to analyze the Stroop effect, 
	a classic result of experimental psychology. Data is compiled from my personal stroop test results. This project uses
	the csv, matplotlib, numpy, pandas, and scipi libraries.


Wrangling OpenStreetMap Data - Uses data munging techniques, 
	such as assessing the quality of the data for validity, accuracy, completeness, consistency and uniformity, 
	to clean the OpenStreetMap data for a part of the world that you care about.