Use R and apply exploratory data analysis techniques to explore relationships in one variable to 
multiple variables and to explore a selected data set for distributions, outliers, and anomalies.
The dataset used in this analysis contains information on the quality of 1599 different red wines.
The name of the dataset file is, 'wineQualityReds.csv'