The file called, 'Udacity_Create_a_Tableau_Story.pdf' contains a link to a Tableau workbook, summary, and design process
Using the baseball data set which contains 1,157 players 
(737 right handed, 316 left handed, and 104 both handed) I created a story that shows my exploration of the data to determine 
if left, right, or both handed hitters have a higher average performance.

https://public.tableau.com/profile/chris.boone#!/vizhome/UdacityCreateaTableauStory-BaseballDataSetv2/BaseballPlayerPerformanceBasedOnHandedness